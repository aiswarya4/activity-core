<?php
namespace com\linways\core\starter\request;

use com\linways\base\request\BaseRequest;
/**
 * @author aiswarya <aiswarya@linways.com>
 * define Request for listing Todo  
 */

class GetTodoListRequest extends BaseRequest
{   
    /**
     * 
     *
     * @var integer
     */
    public $userId;

    /**
     * 
     *
     * @var String
     */
    public $todoName;

    /**
     * 
     *
     * @var String
     */
    public $todoStatus;
}
