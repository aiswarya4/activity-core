<?php
namespace com\linways\core\starter\dto;

use com\linways\base\dto\BaseDTO;
/**
 * create Todo DTO 
 */
class Todo extends BaseDTO
{
	/**
	 *
	 * @var int
	 */
    public $id;
    
	/**
	 *
	 * @var string
	 */
    public $todoName;

    /**
	 *
	 * @var string
	 */
    public $todoStatus;

	/**
	 *
	 * @var int
	 */
	public $userId;
	

	/**
	 *
	 * @var User
	 */
    public $user;
    
}
