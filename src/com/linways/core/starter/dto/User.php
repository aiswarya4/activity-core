<?php
namespace com\linways\core\starter\dto;

use com\linways\base\dto\BaseDTO;
/**
 * create user DTO 
 */
class User extends BaseDTO
{
	/**
	 * user id
	 *
	 * @var int
	 */
	public $id;
	/**
	 * user name
	 *
	 * @var string
	 */
	public $userName;
	/**
	 * date of birth of user
	 *
	 * @var date
	 */
	public $dob;
	/**
	 * email id of user
	 *
	 * @var string
	 */
	public $email;
	/**
	 * password
	 *
	 * @var string
	 */
	public $password;

}
?>