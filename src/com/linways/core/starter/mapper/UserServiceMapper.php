<?php
namespace com\linways\core\starter\mapper;

use com\linways\base\mapper\Result;
use com\linways\base\mapper\IMapper;
use com\linways\base\mapper\ResultMap;
use com\linways\base\util\MakeSingletonTrait;
/**
 * define user service mapper
 */

class UserServiceMapper implements IMapper
{

    use MakeSingletonTrait;
    private $mapper = [];


    const GET_USER_DETAILS = "GET_USER_DETAILS";

    public function getMapper()
    {
        if (empty ($this->mapper)) {
            $this->mapper[self:: GET_USER_DETAILS] = $this->getUserDetails();
        }
        return $this->mapper;
    }



    private function getUserDetails()
    {
        $mapper = null;
        $mapper = new ResultMap('getUserDetails','com\linways\core\starter\dto\User','id','id');
        $mapper->results = [];
        $mapper->results[] = new Result('id','id');
        $mapper->results[] = new Result('userName','userName');
        $mapper->results[] = new Result('dob','dob');
        $mapper->results[] = new Result('email','email');   
        return $mapper;
    }
}