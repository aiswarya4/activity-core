<?php
namespace com\linways\core\starter\mapper;

use com\linways\base\mapper\Result;
use com\linways\base\mapper\IMapper;
use com\linways\base\mapper\ResultMap;
use com\linways\base\util\MakeSingletonTrait;

/**
 * define todo service mapper
 */
class TodoServiceMapper implements IMapper
{

    use MakeSingletonTrait;
    private $mapper = [];


    const GET_TODO_LIST = "GET_TODO_LIST";

    public function getMapper()
    {
        if (empty ($this->mapper)) {
            $this->mapper[self::GET_TODO_LIST] = $this->getTodoList();
        }
        return $this->mapper;
    }



    private function getTodoList()
    {
        $mapper = null;
        $mapper = new ResultMap('getTodoList','com\linways\core\starter\dto\Todo','id','todo_id');
        $mapper->results = [];
        $mapper->results[] = new Result('id','todo_id');
        $mapper->results[] = new Result('todoName','todo_name');
        $mapper->results[] = new Result('todoStatus','todo_status');
        $mapper->results[] = new Result('userId','user_id');
      
        $mapper->results[] = new Result('user', 'user', Result::OBJECT,$this->getUserDetails());

        return $mapper;
    }

    private function getUserDetails()
    {
        $mapper = null;
        $mapper = new ResultMap('getUserDetails','com\linways\core\starter\dto\User','id','user_id');
        $mapper->results = [];
        $mapper->results[] = new Result('id','user_id');
        $mapper->results[] = new Result('userName','user_name');
       
        return $mapper;
    }
}