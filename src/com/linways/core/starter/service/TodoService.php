<?php 
namespace com\linways\core\starter\service; 


use com\linways\core\starter\dto\Todo;
use Respect\Validation\Validator as v;
use com\linways\base\util\MakeSingletonTrait;
use com\linways\core\starter\mapper\TodoServiceMapper;
use com\linways\core\starter\request\GetTodoListRequest;
use com\linways\core\starter\exception\ActivityException;

/**
 * Defining different Todo services 
 */
class TodoService extends BaseService
{

    private $mapper = [];
    use MakeSingletonTrait;

    private function __construct(){
        $this->mapper= TodoServiceMapper::getInstance()->getMapper();
    }

    /**
     * Undocumented function
     *
     * @param GetTodoListRequest $request
     * @return void
     */
    public function getTodoList(GetTodoListRequest $request)
    {
        $todoList = [];
        $request=$this->realEscapeObject($request);
        $requestValidator = v::attribute('requestId',v::stringType());
		$requestValidator->check($request); 
		
        $sql="SELECT t.id as todo_id, t.name as todo_name, t.status as todo_status, u.id as request_id, u.request_name as request_name FROM todo_list t INNER JOIN request u ON t.request_id = u.id WHERE 1=1 ";

        if(!empty($request->requestId))
        {
            $sql.=" AND request_id='$request->requestId'";
        }

        if(!empty($request->todoName))
        {
            $sql.=" AND name LIKE '%$request->todoName%'";
        }
   

		try {

			$todoList= $this->executeQueryForList($sql, $this->mapper[TodoServiceMapper::GET_TODO_LIST]);
		} catch (\Exception $e) {

			throw new ActivityException($e->getCode(),$e->getMessage());
			
		}
		return $todoList;
    }

    /**
     * to create a new todo
     *
     * @param Todo $request
     * @return integer
     */
    public function createTodo($request)
    {
        $sql="";
        $id=NULL;
        
		$request=$this->realEscapeObject($request);//to prevent sql injections
		if(empty($request)){
			throw new ActivityException(ActivityException::FUNCTION_PARAMTER_EMPTY,"Provided TODO is empty");
		}
		$requestValidator = v::attribute('todoName',v::stringType()->length(1,45))
        ->attribute('todoStatus',v::stringType()->length(1,45));
		$requestValidator->check($request); 
	
		$sql="INSERT INTO todo_list(`name`, `status`, `user_id`) VALUES('$request->todoName','$request->todoStatus', '$request->userId')";
		try{
			$id= $this->executeQueryForObject($sql, true);
		}
		catch(\Exception $e)
		{
			throw new ActivityException($e->getCode(),$e->getMessage());
		}
		
		return $id;
	}

}

