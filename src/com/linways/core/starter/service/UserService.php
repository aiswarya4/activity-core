<?php 
namespace com\linways\core\starter\service; 

use com\linways\core\starter\dto\User;
use Respect\Validation\Validator as v;
use com\linways\base\util\MakeSingletonTrait;
use com\linways\core\starter\mapper\UserServiceMapper;
use com\linways\core\starter\exception\ActivityException;

/**
 * Defining different user services
 */
class UserService extends BaseService
{
	use MakeSingletonTrait;

    private $mapper = [];
    

    private function __construct(){
        $this->mapper= UserServiceMapper::getInstance()->getMapper();
    }

	/**
	 * This function used to create a new user. It includes SQL for interting user deatils.
	 *
	 * @param User $user
	 * @return int id of the newly registered user
	 */
	public function createUser($user)
	{		
		$sql="";
		$id=NULL;
		$user=$this->realEscapeObject($user);//to prevent sql injections
		if(empty($user)){
			throw new ActivityException(ActivityException::FUNCTION_PARAMTER_EMPTY,"Provided user details is empty");
			
		}
		$usernameValidator = v::attribute('userName',v::stringType()->length(1,45))
		->attribute('email',v::email())
		->attribute('password',v::alnum()->noWhitespace())
		->attribute('dob',v::date('Y-m-d'));
		$usernameValidator->check($user); 
		$password = md5($user->password);

		$sql="INSERT INTO user(user_name, dob, `email`, password) VALUES('$user->userName','$user->dob', '$user->email', '$password')";
		try{
			$id= $this->executeQueryForObject($sql, true);
		}
		catch(\Exception $e)
		{
			throw new ActivityException($e->getCode(),$e->getMessage());
		}
		
		return $id;
	}
	
	/**
	 * This function used to retrive user details from database from $id.
	 *
	 * @param int $id
	 * @return User $userDetails 
	 */
	public function getUserDetails($id)
	{
		$sql="";
		$userDetails=NULL;

		$id=$this->realEscapeString($id);
		$userValidator=v::stringType()->NotEmpty();
		$userValidator->check($id);
		
		$sql="SELECT id, user_name as userName, dob, email FROM user WHERE id='$id'";

		try {

			$userDetails= $this->executeQueryForObject($sql, false, $this->mapper[UserServiceMapper::GET_USER_DETAILS]);
		} catch (\Exception $e) {

			throw new ActivityException($e->getCode(),$e->getMessage());
			
		}
		return $userDetails;
	}

	/**
	 * this function will return user details from email and password
	 *
	 * @param string $email
	 * @param string $password
	 * @return User $result
	 */
	public function getUserDetailsByEmailAndPassword($email, $password)
	{
		$sql="";
		$result=NULL;

		$email=$this->realEscapeString($email);
		$password=$this->realEscapeString($password);
		$userValidator=v::stringType()->NotEmpty();
		$userValidator->check($email);
		$userValidator->check($password);
	
		
		$sql="SELECT id, user_name as userName, dob, email FROM user WHERE email='$email' AND password=md5('$password')";
		try {
			$result= $this->executeQueryForObject($sql);
		} catch (\Exception $e) {
			throw new ActivityException($e->getCode(),$e->getMessage());
		}
		return $result;

	}

	
}
