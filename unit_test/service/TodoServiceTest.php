<?php

namespace unit_test\service;

use com\linways\core\starter\dto\Todo;
use com\linways\base\test\BaseTestCase;
use com\linways\core\starter\service\TodoService;
use com\linways\core\starter\request\GetTodoListRequest;
use com\linways\core\starter\exception\ActivityException;

class TodoServiceTest extends BaseTestCase
{

    protected function setUp()
    {
        $testDBConfPath = __DIR__ . '/../db_conf/ams_conf.php';
        putenv("TEST_DB_CONFIG=$testDBConfPath");
        putenv("DB_PROFESSIONAL_CONFIG=$testDBConfPath");
        $this->clearDBTable('todo_list');
    }

    public function getTodo(){
        $todo = new Todo();
        
        $todo->todoName = 'review';
 
        $todo->userId = '1';
      
        // NB: Its not a good practice to hard-code the id in any code.
        // This is just for the simplicity of the tutorial
        // $user->role_id = 1; //This is a normal user.
        return $todo;
    }

    public function testGetTodoList()
    {
        $request = new GetTodoListRequest();
        $request->todoName = 'review';
        $request->userId = '1';
      
        $this->setinitialDataUsingSQLFile(__DIR__."/../seed/creatNewTodo.sql");
        
        $todoList = TodoService::getInstance()->getTodoList($request);
        print_r($todoList);
        $this->assertNotEmpty($todoList);
        foreach($todoList as $todoDetails){
            $this->assertObjectHasAttribute('id',$todoDetails);
            $this->assertObjectHasAttribute('todoName',$todoDetails);
            $this->assertObjectHasAttribute('todoStatus',$todoDetails);
        }
      
        // $this->assertObjectHasAttribute('email',$todoDetails);
        // $this->assertNotEmpty($userDetails->userName);
    }
    public function testCreateTodo()
    {
      
            // $this->assertDatabaseHasNot('user',[
            //     "user_name" => $user->userName,
            //     "email" => $user->email
            // ]);
            $tododata = new todo();
            $tododata->todoName = "choose a smeninar topic";
            $tododata->todoStatus = "started";
            $tododata->userId = "1";
            $result = TodoService::getInstance()->createTodo($tododata);
            $this->assertIsInt($result);
            print("result id=".$result);
            

            // $this->assertDatabaseHas('user',[
            //     "user_name" => $user->userName,
            //     "email" => $user->email
            // ]);
            // print("result id=".$result);
    }
}
