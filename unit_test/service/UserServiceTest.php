<?php

namespace unit_test\service;

use com\linways\base\test\BaseTestCase;
use com\linways\core\starter\dto\User;
use com\linways\core\starter\service\UserService;
use com\linways\core\starter\exception\ActivityException;
class UserServiceTest extends BaseTestCase
{
    protected function setUp()
    {
        $testDBConfPath = __DIR__ . '/../db_conf/ams_conf.php';
        putenv("TEST_DB_CONFIG=$testDBConfPath");
        putenv("DB_PROFESSIONAL_CONFIG=$testDBConfPath");
        $this->clearDBTable('user');
    }

    public function getUser(){
        $user = new User();
        $user->id = "1";
        $user->userName = 'aiswarya';
        $user->password = 'secret';
        $user->email = 'aiswaryass@gmail.com';
        $user->dob = '1995-09-07';
        $user->createdBy = "";
        // NB: Its not a good practice to hard-code the id in any code.
        // This is just for the simplicity of the tutorial
        // $user->role_id = 1; //This is a normal user.
        return $user;
    }
    public function testNoSameUserEntryInDatabase()
    {
        $user = $this->getUser();
        $this->assertDatabaseHasNot('user',[
            "user_name" => $user->userName,
            "email" => $user->email
        ]);
    }
    public function testCreateUser()
    {
        $user = $this->getUser();
        // $this->assertDatabaseHasNot('user',[
        //     "user_name" => $user->userName,
        //     "email" => $user->email
        // ]);
        $result = UserService::getInstance()->createUser($user);
        $this->assertIsInt($result);
        $this->assertDatabaseHas('user',[
            "user_name" => $user->userName,
            "email" => $user->email
        ]);
        // print("result id=".$result);
    }
    public function testDuplicateEmailAssertionOnCreateUser()
    {
        $this->expectExceptionCode(ActivityException::DUPLICATE_ENTRY);
        $this->setinitialDataUsingSQLFile(__DIR__."/../seed/creatNewUser.sql");
        $user = $this->getUser();
        $result = UserService::getInstance()->createUser($user);

    }
    public function testCreateUser_forThrowingExceptionOnEmptyParameter()
    {

        $this->expectExceptionCode(ActivityException::FUNCTION_PARAMTER_EMPTY);
       
        UserService::getInstance()->createUser(null);
        
    }
    public function testGetUserDetails()
    {
        $this->setinitialDataUsingSQLFile(__DIR__."/../seed/creatNewUser.sql");
        $user=$this->getUser();
        $userDetails = UserService::getInstance()->getUserDetails($user->id);
        print_r($userDetails);
        $this->assertNotNull($userDetails);
        $this->assertObjectHasAttribute('id',$userDetails);
        $this->assertObjectHasAttribute('userName',$userDetails);
        $this->assertObjectHasAttribute('dob',$userDetails);
        $this->assertObjectHasAttribute('email',$userDetails);
        $this->assertNotEmpty($userDetails->userName);
    }

    public function testGetUserDetailsByEmailAndPassword()
    {
        
        $this->setinitialDataUsingSQLFile(__DIR__."/../seed/creatNewUser.sql");
        $user=$this->getUser();
        
    
        $result = UserService::getInstance()->getUserDetailsByEmailAndPassword($user->email,$user->password);
        print_r($result);
        $this->assertNotEmpty($result);
        $this->assertObjectHasAttribute('id',$result);
        $this->assertObjectHasAttribute('userName',$result);
        $this->assertObjectHasAttribute('dob',$result);
        $this->assertObjectHasAttribute('email',$result);
        $this->assertNotEmpty($result->userName);
      
}

    public function testGetUserDetailsByEmailAndPassword_ForEmptyResult()
    {
        
        $this->setinitialDataUsingSQLFile(__DIR__."/../seed/creatNewUser.sql");
        $user=$this->getUser();
        $user->email='wrong@gmail.com';
        $result = UserService::getInstance()->getUserDetailsByEmailAndPassword($user->email,$user->password);
        print_r($result);
        $this->assertEmpty($result);
     
    }

   

}